from flask import Flask
from app.config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_jwt import JWT

app = Flask(__name__)
app.config.from_object(Config)
db = SQLAlchemy(app)
migrate = Migrate(app, db)

from app import routes, models


def authenticate(username, password):
    user = models.User.query.filter(models.User.username == username).first()
    if user and user.check_password(password):
        return user


def identity(payload):
    user_id = payload['identity']
    return models.User.query.filter(models.User.id == user_id).first()


jwt = JWT(app, authenticate, identity)
