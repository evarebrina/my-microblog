import os


class Config(object):
    DB_HOST = os.getenv('DB_HOST', 'localhost')
    POSTGRES_USER = os.getenv('POSTGRES_USER', 'user1')
    POSTGRES_PASSWORD = os.getenv('POSTGRES_PASSWORD', 'password')
    POSTGRES_DB = os.getenv('POSTGRES_DB', 'microblog_db')
    SQLALCHEMY_DATABASE_URI = os.getenv('DB_URL', f'postgresql://{DB_HOST}:5432/{POSTGRES_DB}?user={POSTGRES_USER}&password={POSTGRES_PASSWORD}')
    SECRET_KEY = os.getenv("SECRET_KEY", "super-secret")
    SQLALCHEMY_TRACK_MODIFICATIONS = os.getenv("SQLALCHEMY_TRACK_MODIFICATIONS", False)
