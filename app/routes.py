from . import app
from app import db
from app.models import User, Thread, Post
from flask import jsonify, make_response, request, abort
from flask_jwt import jwt_required, current_identity


@app.route("/")
def index():
    return "Hello world!"


@app.route("/api/v0.1/threads", methods=["GET"])
def list_threads():
    threads = Thread.query.all()
    result = []
    for thread in threads:
        result.append({
            'id': thread.id,
            'body': thread.body,
            'author': thread.author.username if thread.user_id else None
        })
    return jsonify(result)


@app.route("/api/v0.1/threads", methods=["POST"])
@jwt_required()
def new_thread():
    if not request.json or 'body' not in request.json:
        abort(400)
    thread = Thread(body=request.json.get('body'), user_id=current_identity.id)
    db.session.add(thread)
    db.session.commit()
    return jsonify({
        'status': "You've created a new thread."
    }), 201


@app.route("/api/v0.1/threads/<int:thread_id>")
def get_thread(thread_id: int):
    thread = Thread.query.filter(Thread.id == thread_id).first_or_404()
    posts = []
    for post in thread.posts:
        posts.append({
            'author': post.author.username if post.user_id else None,
            'body': post.body,
            'timestamp': post.timestamp,
        })
    return jsonify({
        'author': thread.author.username if thread.user_id else None,
        'body': thread.body,
        'posts': posts,
        'timestamp': thread.timestamp,
    })


@app.route("/api/v0.1/threads/<int:thread_id>", methods=['POST'])
@jwt_required()
def new_post(thread_id: int):
    if not request.json or 'body' not in request.json:
        abort(400)
    Thread.query.filter(Thread.id == thread_id).first_or_404()
    post = Post(body=request.json.get('body'), thread_id=thread_id, user_id=current_identity.id)
    db.session.add(post)
    db.session.commit()
    return jsonify({
        'status': f"You've created a new post in thread {thread_id}."
    }), 201


@app.route("/sign_up", methods=['POST'])
def new_user():
    if not request.json or 'username' not in request.json or 'email' not in request.json or 'password' not in request.json:
        abort(400)
    if User.query.filter(User.username == request.json.get('username')).first():
        abort(400)
    if User.query.filter(User.email == request.json.get('email')).first():
        abort(400)
    user = User(username=request.json.get('username'), email=request.json.get('email'))
    user.generate_password(request.json.get('password'))
    db.session.add(user)
    db.session.commit()
    return jsonify({
        'status': 'successfully signed up'
    })


@app.errorhandler(404)
def not_found(error):
    return make_response(jsonify({'error': 'nothing here!'}), 404)
